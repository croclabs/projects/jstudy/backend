$(document).ready(() => {
    document.cookie = `jstudy.client.locale=${Intl.DateTimeFormat().resolvedOptions().locale}; Max-Age=604800; SameSite=Strict`
    document.cookie = `jstudy.client.timeZone=${Intl.DateTimeFormat().resolvedOptions().timeZone}; Max-Age=604800; SameSite=Strict`
});