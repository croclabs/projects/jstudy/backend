package io.gitlab.croclabs.jstudy.backend.security;

import io.gitlab.croclabs.jstudy.backend.db.entities.user.UserEntity;
import io.gitlab.croclabs.jstudy.backend.db.entities.user.admin.Admin;
import io.gitlab.croclabs.jstudy.backend.db.entities.user.student.Student;
import io.gitlab.croclabs.jstudy.backend.db.entities.user.teacher.Teacher;
import io.gitlab.croclabs.jstudy.backend.db.repositiories.AdminRepository;
import io.gitlab.croclabs.jstudy.backend.db.repositiories.StudentRepository;
import io.gitlab.croclabs.jstudy.backend.db.repositiories.TeacherRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Component;

@Component("user")
@RequiredArgsConstructor
public class JStudyUserDetailsManager implements UserDetailsManager {
	private final TeacherRepository teacherRepository;
	private final StudentRepository studentRepository;
	private final AdminRepository adminRepository;

	public void saveUser(UserDetails user) {
		if (userExists(user.getUsername())) {
			updateUser(user);
		} else {
			createUser(user);
		}
	}

	@Override
	public void createUser(UserDetails user) {
		if (userExists(user.getUsername())) {
			throw new IllegalArgumentException("User already exists!");
		}

		// reset ID if one is set
		((UserEntity) user).setId(null);

		switch (user) {
			case Teacher t -> teacherRepository.save(t);
			case Student s -> studentRepository.save(s);
			case Admin a -> adminRepository.save(a);
			default -> throw new IllegalStateException("Unexpected value: " + user);
		}
	}

	@Override
	public void updateUser(UserDetails user) {
		if (!userExists(user.getUsername())) {
			throw new IllegalArgumentException("User does not exist!");
		}

		if (((UserEntity) user).getId() == null) {
			throw new IllegalArgumentException("ID cannot be null!");
		}

		switch (user) {
			case Teacher t -> teacherRepository.save(t);
			case Student s -> studentRepository.save(s);
			case Admin a -> adminRepository.save(a);
			default -> throw new IllegalStateException("Unexpected value: " + user);
		}
	}

	@Override
	public void deleteUser(String username) {
		if (teacherRepository.existsByUsername(username)) {
			teacherRepository.deleteByUsername(username);
		} else if (studentRepository.existsByUsername(username)) {
			studentRepository.deleteByUsername(username);
		}
	}

	@Override
	public void changePassword(String oldPassword, String newPassword) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		UserEntity userDetails = (UserEntity) authentication.getPrincipal();
		userDetails.setPassword(newPassword);
		updateUser(userDetails);
	}

	@Override
	public boolean userExists(String username) {
		return
				teacherRepository.existsByUsername(username) ||
				studentRepository.existsByUsername(username) ||
				adminRepository.existsByUsername(username);
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		if (!userExists(username)) {
			throw new UsernameNotFoundException("No user found for username");
		}

		if (teacherRepository.existsByUsername(username)) {
			return teacherRepository.findByUsername(username);
		} else if (studentRepository.existsByUsername(username)) {
			return studentRepository.findByUsername(username);
		} else if (adminRepository.existsByUsername(username)) {
			return adminRepository.findByUsername(username);
		}

		throw new UsernameNotFoundException("No user found for username");
	}

	@SuppressWarnings("unchecked")
	public <T extends UserEntity> T getCurrent() {
		return (T) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}
}
