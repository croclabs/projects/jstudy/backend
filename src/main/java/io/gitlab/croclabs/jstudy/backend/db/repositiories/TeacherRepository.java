package io.gitlab.croclabs.jstudy.backend.db.repositiories;

import io.gitlab.croclabs.jstudy.backend.db.entities.user.teacher.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeacherRepository extends JpaRepository<Teacher, Long> {
	boolean existsByUsername(String username);
	Teacher findByUsername(String username);
	boolean deleteByUsername(String username);
	<T> T findByUsername(String username, Class<T> clazz);
}