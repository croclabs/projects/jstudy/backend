package io.gitlab.croclabs.jstudy.backend.db.entities.user.teacher;

import io.gitlab.croclabs.jstudy.backend.db.entities.test.Test;
import io.gitlab.croclabs.jstudy.backend.db.entities.course.Course;
import io.gitlab.croclabs.jstudy.backend.db.entities.user.UserEntity;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "teacher")
public class Teacher extends UserEntity {
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return Set.of(new SimpleGrantedAuthority("TEACHER"));
	}

	@ManyToMany
	@JoinTable(
			name = "course_teacher",
			joinColumns = @JoinColumn(name = "teacher_id"),
			inverseJoinColumns = @JoinColumn(name = "course_id")
	)
	private Set<Course> courses;

	@OneToMany(mappedBy = "teacher")
	private Set<Test> tests;
}
