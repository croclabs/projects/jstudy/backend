package io.gitlab.croclabs.jstudy.backend.db.repositiories;

import io.gitlab.croclabs.jstudy.backend.db.entities.test.Test;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TestRepository extends JpaRepository<Test, Long> {
}