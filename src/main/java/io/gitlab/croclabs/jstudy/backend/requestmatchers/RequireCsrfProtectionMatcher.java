package io.gitlab.croclabs.jstudy.backend.requestmatchers;

import jakarta.servlet.http.HttpServletRequest;
import org.springframework.security.web.util.matcher.RequestMatcher;

public class RequireCsrfProtectionMatcher implements RequestMatcher {
	@Override
	public boolean matches(HttpServletRequest request) {
		return switch (request.getMethod().toUpperCase()) {
			case "GET", "HEAD", "OPTIONS", "TRACE" -> false;
			default -> request.getHeader("X-API-KEY") == null;
		};
	}
}
