package io.gitlab.croclabs.jstudy.backend.db.entities.user;

import io.gitlab.croclabs.jstudy.backend.db.converters.EncodingConverter;
import io.gitlab.croclabs.jstudy.backend.db.entities.CreatedUpdated;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.core.GrantedAuthority;

import java.net.URL;
import java.util.Collection;
import java.util.HashSet;

@Getter
@Setter
@MappedSuperclass
public class UserEntity implements User {
	@Id
	@GeneratedValue
	@Column(name = "id", nullable = false)
	private Long id;

	@Column(unique = true)
	private String username;

	private String firstname;
	private String lastname;

	@Column(unique = true)
	private String email;

	@RestResource(exported = false)
	@Convert(converter = EncodingConverter.class)
	private String password;

	private boolean active = true;
	private URL avatar;

	@Embedded
	private CreatedUpdated createdUpdated;

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return new HashSet<>();
	}

	@Override
	public boolean isAccountNonExpired() {
		return isEnabled();
	}

	@Override
	public boolean isAccountNonLocked() {
		return isEnabled();
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return isEnabled();
	}

	@Override
	public boolean isEnabled() {
		return active;
	}
}
