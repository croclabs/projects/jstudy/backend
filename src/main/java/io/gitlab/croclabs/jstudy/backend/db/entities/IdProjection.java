package io.gitlab.croclabs.jstudy.backend.db.entities;

public interface IdProjection {
	Long getId();
}
