package io.gitlab.croclabs.jstudy.backend.db.repositiories;

import io.gitlab.croclabs.jstudy.backend.db.entities.user.admin.Admin;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdminRepository extends JpaRepository<Admin, Long> {
	boolean existsByUsername(String username);
	Admin findByUsername(String username);
	boolean deleteByUsername(String username);
	<T> T findByUsername(String username, Class<T> clazz);
}
