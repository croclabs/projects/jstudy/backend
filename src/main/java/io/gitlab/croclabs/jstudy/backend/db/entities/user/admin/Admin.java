package io.gitlab.croclabs.jstudy.backend.db.entities.user.admin;

import io.gitlab.croclabs.jstudy.backend.db.entities.user.UserEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "admin")
public class Admin extends UserEntity {
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return Set.of(new SimpleGrantedAuthority("ADMIN"));
	}
}
