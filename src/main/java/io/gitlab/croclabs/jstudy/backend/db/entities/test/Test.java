package io.gitlab.croclabs.jstudy.backend.db.entities.test;

import io.gitlab.croclabs.jstudy.backend.db.entities.CreatedUpdated;
import io.gitlab.croclabs.jstudy.backend.db.entities.user.teacher.Teacher;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "test")
public class Test {
	@Id
	@GeneratedValue
	@Column(name = "id", nullable = false)
	private Long id;

	@ManyToOne
	@JoinColumn(name="teacher_id", nullable=false)
	private Teacher teacher;

	@Embedded
	private CreatedUpdated createdUpdated;
}