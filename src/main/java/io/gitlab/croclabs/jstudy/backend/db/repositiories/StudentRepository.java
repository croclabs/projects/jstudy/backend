package io.gitlab.croclabs.jstudy.backend.db.repositiories;

import io.gitlab.croclabs.jstudy.backend.db.entities.user.student.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Long> {
	boolean existsByUsername(String username);
	Student findByUsername(String username);
	boolean deleteByUsername(String username);
	<T> T findByUsername(String username, Class<T> clazz);
}