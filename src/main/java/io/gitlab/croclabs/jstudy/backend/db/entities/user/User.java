package io.gitlab.croclabs.jstudy.backend.db.entities.user;

import io.gitlab.croclabs.jstudy.backend.db.entities.CreatedUpdated;
import org.springframework.security.core.userdetails.UserDetails;

import java.net.URL;

public interface User extends UserDetails {
	Long getId();
	String getUsername();
	String getFirstname();
	String getLastname();
	String getEmail();
	URL getAvatar();
	CreatedUpdated getCreatedUpdated();
}
