package io.gitlab.croclabs.jstudy.backend.db.entities.course;

import io.gitlab.croclabs.jstudy.backend.db.entities.CreatedUpdated;
import io.gitlab.croclabs.jstudy.backend.db.entities.user.student.Student;
import io.gitlab.croclabs.jstudy.backend.db.entities.user.teacher.Teacher;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "course")
public class Course {
	@Id
	@GeneratedValue
	@Column(name = "id", nullable = false)
	private Long id;
	private String name;

	@ManyToMany(mappedBy = "courses")
	private Set<Student> students;
	@ManyToMany(mappedBy = "courses")
	private Set<Teacher> teachers;

	@Embedded
	private CreatedUpdated createdUpdated;
}