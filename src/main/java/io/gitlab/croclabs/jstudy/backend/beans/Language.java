package io.gitlab.croclabs.jstudy.backend.beans;

import jakarta.annotation.PostConstruct;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;
import org.springframework.web.servlet.LocaleResolver;

import java.util.*;

@Component
@SessionScope
@Getter
@Setter
@RequiredArgsConstructor
public class Language {
	private final Message msg;
	private final HttpServletRequest request;
	private final HttpServletResponse response;
	private final LocaleResolver localeResolver;

	@Value("${jstudy.supported.languages}")
	private List<String> langs = new ArrayList<>();
	private String current = "de";

	public Map<String, String> langMap() {
		Map<String, String> langMap = new LinkedHashMap<>();
		langs.forEach(lang -> langMap.put(lang, msg.get("lang." + lang)));
		return langMap;
	}

	public Locale getCurrentLocale() {
		return LocaleContextHolder.getLocale();
	}

	public void setCurrent(String current) {
		this.current = current;
		localeResolver.setLocale(request, response, Locale.of(current));
		LocaleContextHolder.setLocale(Locale.of(current));
	}

	@PostConstruct
	public void initCurrent() {
		this.current = getCurrentLocale().getLanguage();
	}
}
