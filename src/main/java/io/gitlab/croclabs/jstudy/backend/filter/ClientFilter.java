package io.gitlab.croclabs.jstudy.backend.filter;

import io.gitlab.croclabs.jstudy.backend.beans.Client;
import jakarta.servlet.*;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.util.WebUtils;

import java.io.IOException;

@Component
@RequiredArgsConstructor
public class ClientFilter implements Filter {
	private final Client client;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest servletRequest = (HttpServletRequest) request;

		client.setTimeZone(getCookieValue(WebUtils.getCookie(servletRequest, "jstudy.client.timeZone")));
		client.setLocale(getCookieValue(WebUtils.getCookie(servletRequest, "jstudy.client.locale")));

		chain.doFilter(request, response);
	}

	private String getCookieValue(Cookie cookie) {
		if (cookie != null) {
			return cookie.getValue();
		} else {
			return null;
		}
	}
}
