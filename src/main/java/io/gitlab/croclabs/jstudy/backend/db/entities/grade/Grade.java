package io.gitlab.croclabs.jstudy.backend.db.entities.grade;

import io.gitlab.croclabs.jstudy.backend.db.entities.CreatedUpdated;
import io.gitlab.croclabs.jstudy.backend.db.entities.user.student.Student;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "grade")
public class Grade {
	@Id
	@GeneratedValue
	@Column(name = "id", nullable = false)
	private Long id;
	private int points;
	private String name;

	@ManyToOne
	@JoinColumn(name="student_id", nullable=false)
	private Student student;

	@Embedded
	private CreatedUpdated createdUpdated;
}