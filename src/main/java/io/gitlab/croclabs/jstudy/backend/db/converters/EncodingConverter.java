package io.gitlab.croclabs.jstudy.backend.db.converters;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;

@Converter
@RequiredArgsConstructor
public class EncodingConverter implements AttributeConverter<String, String> {
	private final PasswordEncoder encoder;

	@Override
	public String convertToDatabaseColumn(String attribute) {
		return encoder.encode(attribute);
	}

	@Override
	public String convertToEntityAttribute(String dbData) {
		return dbData;
	}
}
