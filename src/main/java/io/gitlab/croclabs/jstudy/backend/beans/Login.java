package io.gitlab.croclabs.jstudy.backend.beans;

import jakarta.faces.application.FacesMessage;
import jakarta.faces.context.FacesContext;
import jakarta.faces.view.ViewScoped;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.stereotype.Component;

import java.io.IOException;

@ViewScoped
@Component
@RequiredArgsConstructor
@Getter
@Setter
public class Login {
	private final HttpServletRequest request;
	private final HttpServletResponse response;
	private final AuthenticationManager authManager;
	private final Message msg;
	private final RememberMeServices rememberMeServices;

	private String username;
	private String password;
	private boolean rememberMe;

	public void login() throws IOException {
		UsernamePasswordAuthenticationToken authReq
				= new UsernamePasswordAuthenticationToken(username, password);

		try {
			Authentication auth = authManager.authenticate(authReq);

			rememberMeServices.loginSuccess(request, response, auth);

			SecurityContext sc = SecurityContextHolder.getContext();
			sc.setAuthentication(auth);
			HttpSession session = request.getSession(true);
			session.setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, sc);
			FacesContext.getCurrentInstance().getExternalContext().redirect("/");
		} catch (AuthenticationException e) {
			rememberMeServices.loginFail(request, response);

			FacesContext.getCurrentInstance().
					addMessage("error", new FacesMessage(
							FacesMessage.SEVERITY_ERROR,
							msg.get("error.login"),
							msg.get("error.login.detail")
					));
		}
	}
}
