package io.gitlab.croclabs.jstudy.backend;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.jar.JarFile;

public class ExtensionLoader extends URLClassLoader {
	private JarFile jf = null;

	public ExtensionLoader(File file) throws IOException {
		this(new URL[]{file.toURI().toURL()});
		jf = new JarFile(file.getAbsolutePath());
	}

	public ExtensionLoader(URL[] urls) {
		super(urls, null);
	}

	public <T> T instance() {
		return null;
	}
}
