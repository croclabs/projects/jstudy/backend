package io.gitlab.croclabs.jstudy.backend;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.resource.NoResourceFoundException;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Map;

@ControllerAdvice
@RequiredArgsConstructor
public class NotFoundControllerAdvice {
	private final HttpServletRequest request;
	private final Environment env;

	@ExceptionHandler({NoResourceFoundException.class, NoHandlerFoundException.class})
	public Object handle404() {
		if (request.getRequestURI().startsWith("/api")) {
			return new ResponseEntity<>(
					Map.of(
							"utc", Instant.now().toString(),
							"local", LocalDateTime.now().toString(),
							"status", Map.of(
									"name", HttpStatus.NOT_FOUND.name(),
									"value", HttpStatus.NOT_FOUND.value()
							)
					),
					HttpStatus.NOT_FOUND
			);
		}

		String frontendType = env.getProperty("jstudy.frontend.type");
		if (frontendType == null || frontendType.equals("JSF")) {
			if (request.getRequestURI().equals("/")) {
				return "index.xhtml";
			}

			String[] parts = request.getRequestURI().split("/");
			String target = parts[parts.length - 1];
			return target + ".xhtml";
		} else {
			return "index";
		}
	}
}
