package io.gitlab.croclabs.jstudy.backend.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;

import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Configuration
@Getter
@Setter
public class ResourcesConfig implements WebMvcConfigurer {
	@Value("${jstudy.messages.paths}")
	private List<String> messagesPaths = new ArrayList<>();

	@Value("${jstudy.frontend.type}")
	private String frontendType = "JSF";

	@Value("${jstudy.messages.reload.interval}")
	private int messagesReloadInterval = 0;

	@Bean
	public MessageSource messageSource() {
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messagesPaths.add("classpath:messages/messages");
		messagesPaths.add("file:messages/messages");

		messageSource.setBasenames(messagesPaths.toArray(new String[]{}));

		if ("true".equals(System.getProperty("dev"))) {
			messageSource.setCacheSeconds(0);
		} else {
			messageSource.setCacheSeconds(messagesReloadInterval);
		}

		messageSource.setUseCodeAsDefaultMessage(true);
		messageSource.setDefaultEncoding(StandardCharsets.UTF_8.name());

		return messageSource;
	}

	@Bean
	@ConditionalOnProperty(value="jstudy.frontend.type", havingValue = "JSF", matchIfMissing = true)
	public LocaleChangeInterceptor localeChangeInterceptor() {
		LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
		lci.setParamName("");
		return lci;
	}

	@Bean
	@ConditionalOnProperty(value="jstudy.frontend.type", havingValue = "SPA")
	public LocaleChangeInterceptor localeChangeInterceptorSPA() {
		LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
		lci.setParamName("lang");
		return lci;
	}

	@Bean
	public LocaleResolver localeResolver() {
		CookieLocaleResolver slr = new CookieLocaleResolver("jstudy.locale");
		slr.setDefaultLocale(Locale.GERMAN);
		slr.setCookieMaxAge(Duration.ofDays(7));
		return slr;
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		if ("SPA".equals(frontendType)) {
			registry.addInterceptor(localeChangeInterceptorSPA());
		} else {
			registry.addInterceptor(localeChangeInterceptor());
		}
	}
}
