package io.gitlab.croclabs.jstudy.backend.db.entities.exercise;

import io.gitlab.croclabs.jstudy.backend.db.entities.CreatedUpdated;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "exercise")
public class Exercise {
	@Id
	@GeneratedValue
	@Column(name = "id", nullable = false)
	private UUID id;

	@Embedded
	private CreatedUpdated createdUpdated;
}