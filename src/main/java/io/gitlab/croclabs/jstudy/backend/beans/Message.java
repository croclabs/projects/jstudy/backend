package io.gitlab.croclabs.jstudy.backend.beans;

import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

@Component("msg")
@RequiredArgsConstructor
public class Message {
	private final MessageSource messageSource;

	public String get(String code) {
		return messageSource.getMessage(code, new Object[]{}, LocaleContextHolder.getLocale());
	}

	public String get(String code, Object... args) {
		return messageSource.getMessage(code, args, LocaleContextHolder.getLocale());
	}
}
