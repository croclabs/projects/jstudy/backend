package io.gitlab.croclabs.jstudy.backend.db.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@Embeddable
public class CreatedUpdated implements Serializable {
	@CreationTimestamp
	@Column(updatable = false)
	private LocalDateTime created;
	@UpdateTimestamp
	@Column(insertable = false)
	private LocalDateTime updated;
}