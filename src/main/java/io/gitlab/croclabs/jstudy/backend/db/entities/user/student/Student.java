package io.gitlab.croclabs.jstudy.backend.db.entities.user.student;

import io.gitlab.croclabs.jstudy.backend.db.entities.course.Course;
import io.gitlab.croclabs.jstudy.backend.db.entities.grade.Grade;
import io.gitlab.croclabs.jstudy.backend.db.entities.user.UserEntity;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "student")
public class Student extends UserEntity {
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return Set.of(new SimpleGrantedAuthority("STUDENT"));
	}

	@OneToMany(mappedBy = "student")
	private Set<Grade> grade;

	@ManyToMany
	@JoinTable(
			name = "course_student",
			joinColumns = @JoinColumn(name = "student_id"),
			inverseJoinColumns = @JoinColumn(name = "course_id")
	)
	private Set<Course> courses;
}