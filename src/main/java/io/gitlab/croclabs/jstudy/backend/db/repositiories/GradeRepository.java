package io.gitlab.croclabs.jstudy.backend.db.repositiories;

import io.gitlab.croclabs.jstudy.backend.db.entities.grade.Grade;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GradeRepository extends JpaRepository<Grade, Long> {
}