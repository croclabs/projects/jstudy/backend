package io.gitlab.croclabs.jstudy.backend.beans;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

@Component
@SessionScope
@Getter
@Setter
public class Theme {
	@Value("${jstudy.default.theme}")
	private String name = "primeone";
	@Value("${jstudy.default.theme.mode}")
	private String mode = "dim";

	/**
	 * This is an array of ALL themes from the classpath
	 */
	@Value("classpath:static/themes/*")
	@Setter(AccessLevel.NONE)
	private Resource[] resources;
}
