package io.gitlab.croclabs.jstudy.backend.beans;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

@SessionScope
@Component
@Setter
@Getter
public class Client {
	private String locale;
	private String timeZone;
}
