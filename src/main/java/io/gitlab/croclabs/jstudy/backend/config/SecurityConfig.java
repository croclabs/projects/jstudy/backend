package io.gitlab.croclabs.jstudy.backend.config;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import io.gitlab.croclabs.jstudy.backend.db.entities.user.UserEntity;
import io.gitlab.croclabs.jstudy.backend.db.entities.user.admin.Admin;
import io.gitlab.croclabs.jstudy.backend.db.entities.user.student.Student;
import io.gitlab.croclabs.jstudy.backend.db.entities.user.teacher.Teacher;
import io.gitlab.croclabs.jstudy.backend.requestmatchers.RequireCsrfProtectionMatcher;
import io.gitlab.croclabs.jstudy.backend.security.JStudyUserDetailsManager;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.security.servlet.PathRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.annotation.web.configurers.HeadersConfigurer.FrameOptionsConfig;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.security.web.authentication.rememberme.TokenBasedRememberMeServices;
import org.springframework.security.web.authentication.rememberme.TokenBasedRememberMeServices.RememberMeTokenAlgorithm;

import java.util.List;

@Configuration
@Getter
@Setter
public class SecurityConfig {

	@Value("${jstudy.remember.me.secret}")
	private String rememberMeSecret = "changeMe";

	@Value("${jstudy.admin.username}")
	private String adminUsername = "admin";
	@Value("${jstudy.admin.password}")
	private String adminPassword = "changeMe";
	@Value("${jstudy.admin.firstname}")
	private String adminFirstname = "JStudy";
	@Value("${jstudy.admin.lastname}")
	private String adminLastname = "Admin";

	@Bean
	@ConditionalOnProperty(value="jstudy.frontend.type", havingValue = "JSF", matchIfMissing = true)
	protected SecurityFilterChain configure(HttpSecurity http, RememberMeServices rememberMeServices, Environment env) throws Exception {
		http
				.authorizeHttpRequests(conf -> conf.requestMatchers(
						"/login.xhtml",
						"/themes/**",
						"/js/**", "/scripts/**",
						"/css/**", "/styles/**",
						"/images/**", "/img/**",
						"/jakarta.faces.resource/**"
				).permitAll())
				.csrf(AbstractHttpConfigurer::disable);

		return defaultSettings(http, rememberMeServices).build();
	}

	@Bean
	@ConditionalOnProperty(value="jstudy.frontend.type", havingValue = "SPA")
	protected SecurityFilterChain configureSpa(HttpSecurity http, RememberMeServices rememberMeServices, Environment env) throws Exception {
		http
				.csrf(conf -> conf
					.requireCsrfProtectionMatcher(new RequireCsrfProtectionMatcher())
					.ignoringRequestMatchers(PathRequest.toH2Console()));


		return defaultSettings(http, rememberMeServices).build();
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	public Admin admin(JStudyUserDetailsManager userDetailsManager) {
		Admin admin;

		try {
			admin = (Admin) userDetailsManager.loadUserByUsername(adminUsername);
			admin.setFirstname(adminFirstname);
			admin.setLastname(adminLastname);
			admin.setUsername(adminUsername);
			admin.setPassword(adminPassword);
			userDetailsManager.updateUser(admin);
		} catch (UsernameNotFoundException e) {
			admin = new Admin();
			admin.setFirstname(adminFirstname);
			admin.setLastname(adminLastname);
			admin.setUsername(adminUsername);
			admin.setPassword(adminPassword);
			userDetailsManager.createUser(admin);
		}

		return admin;
	}

	@Bean
	@Profile("dev")
	public List<UserEntity> devUsers(JStudyUserDetailsManager userDetailsManager) {
		Teacher t;
		Student s;

		try {
			t = (Teacher) userDetailsManager.loadUserByUsername("teacher");
			userDetailsManager.updateUser(t);
		} catch (UsernameNotFoundException e) {
			t = new Teacher();
			t.setFirstname("Dev");
			t.setLastname("Teacher");
			t.setUsername("teacher");
			t.setPassword("teacher");
			userDetailsManager.createUser(t);
		}

		try {
			s = (Student) userDetailsManager.loadUserByUsername("student");
			userDetailsManager.updateUser(s);
		} catch (UsernameNotFoundException e) {
			s = new Student();
			s.setFirstname("Dev");
			s.setLastname("Student");
			s.setUsername("student");
			s.setPassword("student");
			userDetailsManager.createUser(s);
		}

		return List.of(t, s);
	}

	@Bean
	public Algorithm jwtAlgorithm(Environment env) {
		return Algorithm.HMAC256(env.getProperty("jstudy.jwt.secret", "secret"));
	}

	@Bean
	public JWTVerifier jwtVerifier(Algorithm algorithm, Environment env) {
		return JWT.require(algorithm)
				.withIssuer(env.getProperty("jstudy.jwt.issuer", "JStudy"))
				.build();
	}

	@Bean
	public AuthenticationManager authenticationManager(AuthenticationConfiguration authenticationConfiguration) throws Exception {
		return authenticationConfiguration.getAuthenticationManager();
	}

	@Bean
	public RememberMeServices rememberMeServices(UserDetailsManager manager) {
		TokenBasedRememberMeServices services = new TokenBasedRememberMeServices(rememberMeSecret, manager, RememberMeTokenAlgorithm.SHA256);
		services.setParameter("remember-me_input");
		services.setCookieName("jstudy.remember.me");
		services.setTokenValiditySeconds(604800);
		return services;
	}

	private HttpSecurity defaultSettings(HttpSecurity http, RememberMeServices rememberMeServices) throws Exception {
		return http
				.authorizeHttpRequests(conf -> conf.requestMatchers(PathRequest.toH2Console()).permitAll()
						.requestMatchers("/login").permitAll()
						.anyRequest().authenticated())
				.headers(conf -> conf.frameOptions(FrameOptionsConfig::sameOrigin))
				.formLogin(conf -> conf
						.loginPage("/login")
						.successForwardUrl("/"))
				.rememberMe(rememberMe -> rememberMe
						.key(rememberMeSecret)
						.rememberMeParameter("remember-me_input")
						.rememberMeServices(rememberMeServices)
						.tokenValiditySeconds(604800));
	}
}
